/****************************************************************************
 *                                                                          *
 *                      Cristian Casadei matr. 0000825714                   *
 *                                                                          *
 ****************************************************************************/


 
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

#define BLKDIM 1024
#define TRUE 1
#define FALSE 0
#define SHARDE_MEMORY 49152
#define CONSTANT_MEMORY 65536
#define SIZEOF_FLOAT 4

__constant__ float P_local[CONSTANT_MEMORY / SIZEOF_FLOAT];
typedef struct
{
    float *P; /* coordinates P[i][j] of point i               */
    int N;    /* Number of points (rows of matrix P)          */
    int D;    /* Number of dimensions (columns of matrix P)   */
} points_t;

/**
 * Returns the largest power of two contained in N
 */
int pw_LQ_2(int N)
{
    int r = N - 1;
    r |= r >> 1;
    r |= r >> 2;
    r |= r >> 4;
    r |= r >> 8;
    r |= r >> 16;
    r++;
    if (r == N)
        return r;
    r = r >> 1;
    return r;
}

/**
 * initializes the index_dominat vector to its initial value:
 * index_dominant [0] = 0
 * index_dominant [1] = 1
 * index_dominant [2] = 2
 * index_dominant [3] = 3
 * index_dominant [4] = 4
 * ...
 * index_dominant [size - 1] = size - 1
 */
__global__ void cudaMemeset_index_dominant(int *index_dominant, int size)
{
    int my_id = threadIdx.x + blockIdx.x * blockDim.x;
    if (my_id < size)
        index_dominant[my_id] = my_id;
}

/**
 * initializes the vector to its initial value:
 * index dominant [0] = 0
 * index_dominant [1] = 1
 * index dominant [2] = 1
 * index_dominant [3] = 1
 * index_dominant [4] = 1
 * ...
 * index_dominant [size - 1] = 1
 */
__global__ void cudaMemeset_s(int *s, int size)
{
    int my_id = threadIdx.x + blockIdx.x * blockDim.x;
    if (my_id < size)
        s[my_id] = 1;
}

/**
 * perform the reduction of large portions BLKDIM * 2 elements of the input 
 * factor, return the results in the output vector
 *
 * Code reference:
 * https://developer.download.nvidia.com/assets/cuda/files/reduction.pdf
 *
 */
__global__ void reduction_per_block(const int *input, int pw_LQ_2_last, 
                                    const int size,
                                    int *output)
{
    extern __shared__ int s_out[];
    int tid = threadIdx.x + blockIdx.x * blockDim.x * 2;
    s_out[threadIdx.x] = 0;
    __syncthreads();
    if (tid < size)
    {
        if (pw_LQ_2_last != 0 && blockIdx.x == gridDim.x - 1)
        {
            if (threadIdx.x >= pw_LQ_2_last)
            {
                s_out[threadIdx.x - pw_LQ_2_last] = input[tid];
            }
            else if (pw_LQ_2_last == blockDim.x && tid + pw_LQ_2_last < size)
            {
                s_out[threadIdx.x] = input[tid + pw_LQ_2_last];
            }
            __syncthreads();
            if (threadIdx.x < pw_LQ_2_last)
            {
                s_out[threadIdx.x] = input[tid] + s_out[threadIdx.x];
            }
        }
        else
        {
            pw_LQ_2_last = blockDim.x;
            if (threadIdx.x < pw_LQ_2_last)
            {
                s_out[threadIdx.x] = input[tid] + input[tid + pw_LQ_2_last];
            }
        }
        __syncthreads();

        if (pw_LQ_2_last >= 1024)
        {
            if (threadIdx.x < 512)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 512];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 512)
        {
            if (threadIdx.x < 256)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 256];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 256)
        {
            if (threadIdx.x < 128)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 128];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 128)
        {
            if (threadIdx.x < 64)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 64];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 64)
        {
            if (threadIdx.x < 32)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 32];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 32)
        {
            if (threadIdx.x < 16)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 16];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 16)
        {
            if (threadIdx.x < 8)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 8];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 8)
        {
            if (threadIdx.x < 4)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 4];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 4)
        {
            if (threadIdx.x < 2)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 2];
            }
            __syncthreads();
        }
        if (pw_LQ_2_last >= 2)
        {
            if (threadIdx.x < 1)
            {
                s_out[threadIdx.x] += s_out[threadIdx.x + 1];
            }
        }
        __syncthreads();
    }
    if (threadIdx.x == 0)
    {
        output[blockIdx.x] = s_out[0];
    }
}

/**
 * 
 * relative_index_dominant_exec & relative_index_dominant_exec
 *
 *
 * reading the vector d_s it calculates the new values of 
 * d_relative_index_dominant, to optimize the calculation the values of 
 * d_relative_index_dominant itself are also used. For example:
 * 
 *                       d_s = 1, 0, 0, 0, 0, 0, 1, 0, 1, 1
 * d_relative_index_dominant = 0, 5, 4, 3, 2, 1, 0, 1, 0, 0
 */
__global__ void relative_index_dominant_load(
                                        const int *d_relative_index_dominant,  
                                        const int N, 
                                        int *d_last_rid)
{
    const int my_id = threadIdx.x + blockIdx.x * BLKDIM;
    if (my_id < N)
        d_last_rid[my_id] = d_relative_index_dominant[my_id];
}
__global__ void relative_index_dominant_exec(const int *d_last_rid, 
                                                const int *d_s, 
                                                const int N, 
                                                int *d_relative_index_dominant)
{
    const int my_id = threadIdx.x + blockIdx.x * BLKDIM;
    if (my_id < N)
    {
        int i = my_id;
        for (int j = 0; j < N; j++)
        {
            i = i + d_last_rid[i % N];
            if (d_s[(j + my_id) % N] == 1)
            {
                d_relative_index_dominant[my_id] = j;
                break;
            }
            i++;
        }
    }
}

/**
 * 
 * index_dominant_repopulation_loading & index_dominant_repopulation__exec
 * 
 * updates the vector index_dominant_repopulation which directly depends on the 
 * relative_index_dominant (d_rid) vector which depends directly on s. The 
 * index_dominant_repopulation contains the value of all dominant indexes in 
 * ascending order. For example:
 *
 * d_s[0] = 1; d_index_dominant[0] = 0; 
 * d_s[1] = 0; d_index_dominant[1] = 4; 
 * d_s[2] = 0; d_index_dominant[2] = 6;                          
 * d_s[3] = 0; d_index_dominant[3] = 7;
 * d_s[4] = 1; ...
 * d_s[5] = 0; 
 * d_s[6] = 1; 
 * d_s[7] = 1; 
 * ... 
 */
__global__ void index_dominant_repopulation_loading(
                                                const int points_costant, 
                                                const int points_shared, 
                                                const int *d_index_dominant, 
                                                const int r, 
                                                const int *d_rid, 
                                                int *d_constant_start_local, 
                                                int *d_start, 
                                                int *d_end)
{
    int start_dominant = blockIdx.x * points_shared;
    d_start[blockIdx.x] = d_index_dominant[start_dominant];
    if (start_dominant + points_shared >= r)
        d_end[blockIdx.x] = d_index_dominant[r - 1] + 1;
    else
        d_end[blockIdx.x] = d_index_dominant[start_dominant + points_shared];
    if (blockIdx.x == 0)
    {
        if (*d_constant_start_local + points_costant > r)
        {
            *d_constant_start_local = d_index_dominant[r - 1] + 1;
        }
        else
        {
            *d_constant_start_local = 
                d_index_dominant[
                            (*d_constant_start_local) + points_costant] + 
                d_rid[d_index_dominant[
                            (*d_constant_start_local) + points_costant]]; 
        }
    }
}
__global__ void index_dominant_repopulation_exec(const int *start_write, 
                                                    const int *d_rid, 
                                                    const int *d_start, 
                                                    const int *d_end, 
                                                    int *d_constant_start_local, 
                                                    int *d_index_dominant)
{
    int my_start = d_start[blockIdx.x];
    int my_end = d_end[blockIdx.x];
    int i = start_write[blockIdx.x];
    int flag = TRUE;
    my_start = my_start + d_rid[my_start];

    if (my_start <= *d_constant_start_local && my_end > *d_constant_start_local)
    {
        while (my_end > my_start)
        {
            if (my_start == *d_constant_start_local && flag == TRUE)
            {
                flag = FALSE;
                *d_constant_start_local = i;
            }
            d_index_dominant[i] = my_start++;
            my_start = my_start + d_rid[my_start];
            i++;
        }
    }
    else
    {
        while (my_end > my_start)
        {
            d_index_dominant[i] = my_start++;
            my_start = my_start + d_rid[my_start];
            i++;
        }
    }
}

/**
 * performs the "exclusive scan" of large vector portions BLKDIM * 2 and 
 * returns the result in g_odata. last_element would be the last element of the 
 * inclusive scan
 *
 * Code reference: 
 * https://developer.nvidia.com/gpugems/gpugems3/part-vi-gpu-computing/chapter-39-parallel-prefix-sum-scan-cuda
 *
*/
__global__ void scan(const int *g_idata, int pw_LQ_2, const int n, 
                        const int *sum_block, const int *sum_all, int *g_odata, 
                        int *last_element)
{
    extern __shared__ int temp[]; // allocated on invocation

    if (blockIdx.x < gridDim.x - 1 || pw_LQ_2 == 0)
    {
        pw_LQ_2 = blockDim.x * 2;
    }

    if (2 * threadIdx.x < pw_LQ_2)
    {
        int offeset_global = blockIdx.x * blockDim.x * 2;
        int offset = 1;
        int sum_any = sum_block[blockIdx.x] + (*sum_all);
        temp[2 * threadIdx.x] = g_idata[2 * threadIdx.x + offeset_global]; // load input into shared memory
        temp[2 * threadIdx.x + 1] = g_idata[2 * threadIdx.x + 1 + offeset_global];
        for (int d = pw_LQ_2 >> 1; d > 0; d >>= 1) // build sum in place up the tree
        {
            __syncthreads();
            if (threadIdx.x < d)
            {
                int ai = offset * (2 * threadIdx.x + 1) - 1; //-1 perche gli array partono da 0
                int bi = offset * (2 * threadIdx.x + 2) - 1;
                temp[bi] += temp[ai];
            }
            offset *= 2;
        }

        if (threadIdx.x == 0)
        {
            if (blockIdx.x == gridDim.x - 1 && pw_LQ_2 == blockDim.x * 2)
            {
                *last_element = temp[pw_LQ_2 - 1] + sum_any;
            }
            temp[pw_LQ_2 - 1] = 0;
        } // clear the last element

        for (int d = 1; d < pw_LQ_2; d *= 2) // traverse down tree & build scan
        {
            offset >>= 1;
            __syncthreads();
            if (threadIdx.x < d)
            {
                int ai = offset * (2 * threadIdx.x + 1) - 1;
                int bi = offset * (2 * threadIdx.x + 2) - 1;
                int t = temp[ai];
                temp[ai] = temp[bi];
                temp[bi] += t;
            }
        }

        __syncthreads();
        g_odata[offeset_global + 2 * threadIdx.x] = temp[2 * threadIdx.x] + sum_any; // write results to device memory
        g_odata[offeset_global + 2 * threadIdx.x + 1] = temp[2 * threadIdx.x + 1] + sum_any;
    }

    __syncthreads();
    if (threadIdx.x == 0 && blockIdx.x == gridDim.x - 1 && pw_LQ_2 != blockDim.x * 2)
    {
        pw_LQ_2 = blockDim.x * 2 * blockIdx.x + pw_LQ_2;
        for (int i = pw_LQ_2; i < n; i++)
        {
            g_odata[i] = g_odata[i - 1] + g_idata[i - 1];
        }
        *last_element = g_odata[n - 1] + g_idata[n - 1];
    }
}

/**
 * this is the function called by the host to execute the skyline operator on  
 * the divece
 */
__global__ void skyline_kernel(const float *P, const int D, 
                                const int r, const int *index_dominant, 
                                const int c_P_size, int *r_partial, int *s)
{
    extern __shared__ float s_P[];//shared points
    //index of index dominant
    const int tid = threadIdx.x + blockIdx.x * blockDim.x; 
    const int tid_re = threadIdx.x; //point index in shared
    //index of the first size of the point loaded in shared memory
    const int s_P_id = tid_re * D;  
    //index of point P that I am processing and that I loaded in shared memory
    int P_id;   
    int i, j; 

    //initialization of variables
    int s_P_size = blockDim.x;
    if (blockIdx.x == gridDim.x - 1 && r % blockDim.x != 0)
    {
        s_P_size = r % blockDim.x;
    }

    if (threadIdx.x == 0)
    {
        r_partial[blockIdx.x] = s_P_size;
    }

    //loading data into shared memory
    if (tid_re < s_P_size)
    {
        P_id = index_dominant[tid];
        for (i = 0; i < D; i++)
        {
            s_P[s_P_id + i] = P[P_id * D + i];
        }
    }

    //running the skyline operator
    __syncthreads();    //so we make sure that r_partial has been initialized
    if (tid_re < s_P_size)
    {
        int cmp;
        for (i = 0; i < c_P_size; i++)
        {
            cmp = 0;

            // in the serial program this would be the dominant function
            for (j = 0; j < D; j++)
            {
                //p < q
                cmp |= P_local[i * D + j] < s_P[s_P_id + j] ? 1 : 0;
                //p > q
                cmp |= P_local[i * D + j] > s_P[s_P_id + j] ? 2 : 0;
                if (cmp == 1 || cmp == 3)
                    break;
            }
            if (cmp == 2)
            {
                s[P_id] = 0;
                atomicSub(&r_partial[blockIdx.x], 1);
                break;
            }
        }
    }
}

/**
 * this is the function called by the host to execute the skyline operator on  
 * the divece. When the points have so many dimensions that they cannot contain 
 * even one point in shared memory
 */
__global__ void skyline_kernel_any_D(const float *P, 
                                        const int D, 
                                        const int N, 
                                        int *s)
{
    /* define vector in shared memory*/
    const int tid = blockIdx.x * D; //index of P
    int cmp;
    for (int i = 0; i < N; i++)
    {
        cmp = 0;
        for (int j = 0; j < D; j++)
        {
            //P < Q
            cmp |= P[i * D + j] < P[tid + j] ? 1 : 0;
            //P > Q
            cmp |= P[i * D + j] > P[tid + j] ? 2 : 0;
            if (cmp == 1 || cmp == 3)
                break;
        }
        if (cmp == 2)
        {
            s[blockIdx.x] = 0;
            break;
        }
    }
}
/**
 * Read input from stdin. Input format is:
 *
 * d [other ignored stuff]
 * N
 * p0,0 p0,1 ... p0,d-1
 * p1,0 p1,1 ... p1,d-1
 * ...
 * pn-1,0 pn-1,1 ... pn-1,d-1
 *
 */
void read_input(points_t *points)
{
    char buf[1024];
    int N, D, i, k;
    float *P;

    if (1 != scanf("%d", &D))
    {
        fprintf(stderr, "FATAL: can not read the dimension\n");
        exit(EXIT_FAILURE);
    }
    assert(D >= 2);
    if (NULL == fgets(buf, sizeof(buf), stdin))
    { /* ignore rest of the line */
        fprintf(stderr, "FATAL: can not read the first line\n");
        exit(EXIT_FAILURE);
    }
    if (1 != scanf("%d", &N))
    {
        fprintf(stderr, "FATAL: can not read the number of points\n");
        exit(EXIT_FAILURE);
    }
    P = (float *)malloc(D * N * sizeof(*P));
    assert(P);
    for (i = 0; i < N; i++)
    {
        for (k = 0; k < D; k++)
        {
            if (1 != scanf("%f", &(P[i * D + k])))
            {
                fprintf(stderr, 
                        "FATAL: failed to get coordinate %d of point %d\n", 
                        k, 
                        i);
                exit(EXIT_FAILURE);
            }
        }
    }
    points->P = P;
    points->N = N;
    points->D = D;
}

void free_points(points_t *points)
{
    free(points->P);
    points->P = NULL;
    points->N = points->D = -1;
}

void CUDA_scan(int *v_in, 
                int scn_size, 
                int *v_rdc, 
                int rdc_size, 
                int *v_zero, 
                int *v_out, 
                int *last_element)
{
    if (scn_size > BLKDIM)
    {
        int grid_scn = BLKDIM;
        int scn_size_it = BLKDIM * BLKDIM;
        int *v_rdc_scan, *none, *sum_all;
        cudaSafeCall(cudaMalloc\
            ((void **)&v_rdc_scan, BLKDIM * sizeof(*v_rdc_scan)));
        cudaSafeCall(cudaMalloc((void **)&none, sizeof(*none)));
        cudaSafeCall(cudaMalloc((void **)&sum_all, sizeof(*sum_all)));
        cudaSafeCall(cudaMemset((void *)sum_all, 0, sizeof(*sum_all)));
        for (int i = 0; i * (BLKDIM * BLKDIM) < scn_size; i++)
        {

            /*************************************************
             *                                               *
             *         phase : scan reduction                *
             *                                               *
             *************************************************/
            if ((i + 1) * BLKDIM > rdc_size)
            {
                grid_scn = rdc_size - (i * BLKDIM);
            }
            if (grid_scn > 1)
            {
                scan<<<1, grid_scn / 2, grid_scn * sizeof(*v_rdc_scan)>>>\
                (&v_rdc[i * BLKDIM], 
                    pw_LQ_2(grid_scn % BLKDIM), 
                    grid_scn, v_zero, &v_zero[0], 
                    v_rdc_scan, none);
                cudaCheckError();
                cudaDeviceSynchronize();
            }
            if ((i + 1) * BLKDIM * BLKDIM > scn_size)
            {
                scn_size_it = scn_size - (i * BLKDIM * BLKDIM);
            }

            /*************************************************
             *                                               *
             *               phase : scan                    *
             *                                               *
             *************************************************/
            scan<<<grid_scn, BLKDIM / 2, BLKDIM * sizeof(*v_out)>>>\
            (&v_in[i * BLKDIM * BLKDIM], 
                pw_LQ_2(scn_size_it % BLKDIM), 
                scn_size_it, 
                v_rdc_scan, 
                sum_all, 
                &v_out[i * BLKDIM * BLKDIM], 
                last_element);
            cudaCheckError();
            cudaSafeCall(cudaMemcpy\
                (sum_all, 
                    last_element, 
                    sizeof(*last_element), 
                    cudaMemcpyDeviceToDevice));
        }
        cudaSafeCall(cudaFree(v_rdc_scan));
        cudaSafeCall(cudaFree(none));
        cudaSafeCall(cudaFree(sum_all));
    }
    else
    {
        /*************************************************
        *                                                *
        *               simple scan                      *
        *                                                *
        **************************************************/
        scan<<<1, BLKDIM / 2, BLKDIM * sizeof(*v_out)>>>\
        (v_in, 
            pw_LQ_2(scn_size), 
            scn_size, 
            v_zero, 
            &v_zero[0], 
            v_out, 
            last_element);
        cudaCheckError();
    }
}

/**
 * Compute the skyline of |points|. At the d_end, s[i] == 1 iff point
 * |i| belongs to the skyline. This function returns the number r of
 * points in to the skyline. The caller is responsible for allocating
 * a suitably sized array |s|.
 */
int skyline(const points_t *points, int *s)
{
    //Host variabile
    const int D = points->D;
    const int N = points->N;
    const float *P = points->P;
    int r, grid_rdc;

    //Device variabile
    float *d_P;
    int *d_r, *d_r_partial, *d_constant_start_local, *d_s, *d_index_dominant, 
    *d_end, *d_start, *d_start_write_idx_dmn, *d_relative_index_dominant, 
    *d_last_rid, *d_rdc_for_scn, *d_v_zero, *d_rdc;

    //Set host variabile
    r = N;
    int points_shared = SHARDE_MEMORY / (sizeof(*d_P) * D);
    if (points_shared > BLKDIM)
    {
        points_shared = BLKDIM;
    }

    //Allocate space on the device
    const int sizeof_points = N * D * sizeof(*P);
    const int sizeof_s = N * sizeof(*s);
    cudaSafeCall(cudaMalloc((void **)&d_P, sizeof_points));
    cudaSafeCall(cudaMalloc((void **)&d_s, sizeof_s));
    cudaSafeCall(cudaMalloc((void **)&d_r, sizeof(r)));

    //Set memory on the device
    cudaSafeCall(cudaMemcpy(d_P, P, sizeof_points, cudaMemcpyHostToDevice));
    cudaSafeCall(cudaMemcpy(d_r, &r, sizeof(r), cudaMemcpyHostToDevice));
    cudaMemeset_s<<<(N + BLKDIM - 1) / BLKDIM, BLKDIM>>>(d_s, N);

    //Launch of the kenred variant if shared memory cannot be used
    if (points_shared == 0)
    {
        skyline_kernel_any_D<<<N, 1>>>(d_P, D, r, d_s);
        cudaCheckError();

        //Copy s back to host memory
        cudaSafeCall(cudaMemcpy(s, d_s, sizeof_s, cudaMemcpyDeviceToHost));

        //Find r
        cudaSafeCall(cudaMalloc((void **)&d_rdc, sizeof(*d_rdc) * N));
        int i = 0;
        do
        {
            grid_rdc = (r + (BLKDIM * 2) - 1) / (BLKDIM * 2);
            if (i % 2 == 0)
            {
                reduction_per_block<<<grid_rdc, BLKDIM, BLKDIM * sizeof(int)>>>\
                (d_s, pw_LQ_2(r % (BLKDIM * 2)), r, d_rdc);
            }
            else
            {
                reduction_per_block<<<grid_rdc, BLKDIM, BLKDIM * sizeof(int)>>>\
                (d_rdc, pw_LQ_2(r % (BLKDIM * 2)), r, d_s);
            }

            cudaCheckError();
            r = r / (BLKDIM * 2) + r % (BLKDIM * 2);
            i++;
        } while (grid_rdc > 1);
        //Copy r back to host memory
        if (i % 2 == 0)
        {
            cudaSafeCall(cudaMemcpy(&r, d_s, sizeof(r), 
                                        cudaMemcpyDeviceToHost));
        }
        else
        {
            cudaSafeCall(cudaMemcpy(&r, 
                                    d_rdc, 
                                    sizeof(r),               
                                    cudaMemcpyDeviceToHost));
        }
    }
    else
    {
        int points_costant = CONSTANT_MEMORY / (sizeof(*d_P) * D);
        int h_constant_start = 0 - points_costant;
        int r_last = N;
        int finish = FALSE;
        int grid_dim_rid = (N + BLKDIM - 1) / BLKDIM;
        int grid_sky = (N + points_shared - 1) / points_shared;
        int h_dominant[N];
        const int shared_memory_dim = points_shared * sizeof(*d_P) * D;

        //Allocate space on the device
        const int sizeof_r_partial = grid_sky * sizeof(*d_r_partial);
        cudaSafeCall(cudaMalloc((void **)&d_index_dominant, sizeof_s));
        cudaSafeCall(cudaMalloc((void **)&d_constant_start_local, 
                                    sizeof(*d_constant_start_local)));
        cudaSafeCall(cudaMalloc((void **)&d_last_rid, sizeof_s));
        cudaSafeCall(cudaMalloc((void **)&d_relative_index_dominant, 
                            sizeof_s + sizeof(d_relative_index_dominant[0])));
        cudaSafeCall(cudaMalloc((void **)&d_r_partial, sizeof_r_partial));
        cudaSafeCall(cudaMalloc((void **)&d_start, sizeof_r_partial));
        cudaSafeCall(cudaMalloc((void **)&d_end, sizeof_r_partial));
        cudaSafeCall(cudaMalloc((void **)&d_rdc_for_scn, sizeof_r_partial));
        cudaSafeCall(cudaMalloc((void **)&d_v_zero, sizeof_r_partial));
        cudaSafeCall(cudaMalloc((void **)&d_start_write_idx_dmn,
                                    sizeof_r_partial));

        //et memory on the device
        cudaSafeCall(cudaMemset((void *)d_constant_start_local, 
                                    0, 
                                    sizeof(*d_constant_start_local)));
        cudaSafeCall(cudaMemset((void *)d_relative_index_dominant, 
                                    0, 
                                    sizeof_s));
        cudaSafeCall(cudaMemset((void *)d_r_partial, 0, sizeof_r_partial));
        cudaSafeCall(cudaMemset((void *)d_v_zero, 0, sizeof_r_partial));
        cudaMemeset_index_dominant<<<(N + BLKDIM - 1) / BLKDIM, BLKDIM>>>\
            (d_index_dominant, N);
        cudaCheckError();
        do
        {
            /*********************************************************
            **                       phase 4                        **
            **    set h_constant_start  &  set  d_index_dominant    **
            **                                                      **
            **********************************************************/
            if (r != r_last)
            {
                relative_index_dominant_load<<<grid_dim_rid, BLKDIM>>>\
                    (d_relative_index_dominant, N, d_last_rid);
                cudaCheckError();
                relative_index_dominant_exec<<<grid_dim_rid, BLKDIM>>>\
                    (d_last_rid, d_s, N, d_relative_index_dominant);
                cudaCheckError();
                index_dominant_repopulation_loading<<<grid_sky, 1>>>\
                    (points_costant, 
                        points_shared, 
                        d_index_dominant, 
                        r_last, 
                        d_relative_index_dominant, 
                        d_constant_start_local, 
                        d_start, 
                        d_end);
                cudaCheckError();
                index_dominant_repopulation_exec<<<grid_sky, 1>>>\
                    (d_start_write_idx_dmn, 
                        d_relative_index_dominant, 
                        d_start, d_end, 
                        d_constant_start_local, 
                        d_index_dominant);
                cudaCheckError();
                r_last = r;
                cudaSafeCall(cudaMemcpy(&h_constant_start, 
                                            d_constant_start_local, 
                                            sizeof(*d_constant_start_local),  
                                            cudaMemcpyDeviceToHost));
            }
            else
            {
                h_constant_start = h_constant_start + points_costant;
                cudaSafeCall(cudaMemcpy(d_constant_start_local, 
                                        &h_constant_start, 
                                        sizeof(r), 
                                        cudaMemcpyHostToDevice));
            }

            /*********************************************************
            **                       phase 1                        **
            **              costant memory set  &  finish           **
            **                                                      **
            **********************************************************/
            if (h_constant_start + points_costant >= r)
            {
                points_costant = r - h_constant_start;
                finish = TRUE;
            }

            if (points_costant > 0)
            {
                cudaSafeCall(cudaMemcpy
                    (h_dominant, 
                        &d_index_dominant[h_constant_start], 
                        points_costant * sizeof(*d_index_dominant), 
                        cudaMemcpyDeviceToHost));
                for (int f = 0; f < points_costant; f++)
                {
                    cudaSafeCall(cudaMemcpyToSymbol(P_local, 
                                                    &P[h_dominant[f] * D],
                                                    sizeof(float) * D, 
                                                    sizeof(float) * D * f,
                                                    cudaMemcpyHostToDevice));
                }

                /*********************************************************
                **                       phase 2                        **
                **                  execution skyline                   **
                **                                                      **
                **********************************************************/
                grid_sky = (r + points_shared - 1) / points_shared;
                skyline_kernel<<<grid_sky, points_shared, shared_memory_dim>>>\
                    (d_P, 
                        D, 
                        r, 
                        d_index_dominant, 
                        points_costant, 
                        d_r_partial, 
                        d_s);
                cudaCheckError();

                /*********************************************************
                **                       phase 3                        **
                **       Find r & prepare scan for index dominant       **
                **                                                      **
                **********************************************************/
                if (grid_sky == 1)
                {
                    cudaSafeCall(cudaMemcpy(&r, 
                                            d_r_partial, 
                                            sizeof(r), 
                                            cudaMemcpyDeviceToHost));
                }
                else
                {
                    grid_rdc = (grid_sky + BLKDIM - 1) / BLKDIM;
                    reduction_per_block\
                        <<<grid_rdc, BLKDIM / 2, BLKDIM * sizeof(int)>>>\
                            (d_r_partial, 
                                pw_LQ_2(grid_sky % BLKDIM), 
                                grid_sky, 
                                d_rdc_for_scn);
                    cudaCheckError();
                    CUDA_scan(d_r_partial, 
                                grid_sky, 
                                d_rdc_for_scn, 
                                grid_rdc, 
                                d_v_zero, 
                                d_start_write_idx_dmn, 
                                d_r);
                    //Copy r back to host memory
                    cudaSafeCall(cudaMemcpy(&r, 
                                            d_r, 
                                            sizeof(r), 
                                            cudaMemcpyDeviceToHost));
                }
            }
        } while (finish == FALSE && points_costant > 0);
        //Copy s back to host memory
        cudaSafeCall(cudaMemcpy(s, d_s, sizeof_s, cudaMemcpyDeviceToHost));
    }
    return r;
}

/**
 * Print the coordinates of points belonging to the skyline |s| to
 * standard ouptut. s[i] == 1 iff point i belongs to the skyline.  The
 * output format is the same as the input format, so that this program
 * can process its own output.
 */
void print_skyline(const points_t *points, const int *s, int r)
{
    const int D = points->D;
    const int N = points->N;
    const float *P = points->P;
    int i, k;

    printf("%d\n", D);
    printf("%d\n", r);
    for (i = 0; i < N; i++)
    {
        if (s[i])
        {
            for (k = 0; k < D; k++)
            {
                printf("%f ", P[i * D + k]);
            }
            printf("\n");
        }
    }
}

int main(int argc, char *argv[])
{
    points_t points;
    if (argc != 1)
    {
        fprintf(stderr, "Usage: %s < input_file > output_file\n", argv[0]);
        return EXIT_FAILURE;
    }

    read_input(&points);
    int *s = (int *)malloc(points.N * sizeof(*s));
    assert(s);
    const double tstart = hpc_gettime();
    const int r = skyline(&points, s);
    const double elapsed = hpc_gettime() - tstart;
    print_skyline(&points, s, r);

    fprintf(stderr,
            "\n\t%d points\n\t%d dimensione\n\t%d points in skyline\n\nExecution time %f seconds\n", 
            points.N, points.D, r, elapsed);
    free_points(&points);
    free(s);
    return EXIT_SUCCESS;
}

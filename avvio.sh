#SIMD
echo "simd"
gcc -std=c99 -Wall -Wpedantic -O2 -D_XOPEN_SOURCE=600 -DNO_SIMD -mavx -fopenmp omp-skyline.c  -lm -o omp-skyline
OMP_NUM_THREADS=11 ./omp-skyline <w13.in >OMP_SIMD_core_11_test_13.txt
OMP_NUM_THREADS=11 ./omp-skyline <test7-N100000-D200.in >OMP_SIMD_core_11_test_7.txt
OMP_NUM_THREADS=11 ./omp-skyline <zero-N60000-D100.in >OMP_SIMD_core_11_test_zero_100.txt
OMP_NUM_THREADS=12 ./omp-skyline <w13.in >OMP_SIMD_core_12_test_13.txt


#NO SIMD
echo "omp"
gcc -std=c99 -Wall -Wpedantic -O2 -D_XOPEN_SOURCE=600 -DNO_SIMD -mavx -fopenmp omp-skyline.c  -lm -o omp-skyline
OMP_NUM_THREADS=11 ./omp-skyline <w13.in >OMP_core_11_test_13.txt
OMP_NUM_THREADS=11 ./omp-skyline <test7-N100000-D200.in >OMP_core_11_test_7.txt
OMP_NUM_THREADS=11 ./omp-skyline <zero-N60000-D100.in >OMP_core_11_test_zero_100.txt
OMP_NUM_THREADS=12 ./omp-skyline <w13.in >OMP_core_12_test_13.txt

#CUDA
echo "cuda"
cc -std=c99 -Wall -Wpedantic -O2 -D_XOPEN_SOURCE=600 -DNO_SIMD -mavx -fopenmp omp-skyline.c  -lm -o omp-skyline
./cuda-skyline <w13.in >CUDA_test_13.txt
./cuda-skyline <test7-N100000-D200.in >CUDA_test_7.txt
./cuda-skyline <zero-N60000-D100.in >CUDA_test_zero_100.txt


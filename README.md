# Compilazione

Per compialre i codici si pùò utilizare il makefile utilizzando il comando make.
Per compilare la versione OMP senza l'utilizzo delle istruzioni SIMD definire il
simbolo NO_SIMD in fase di compilazione nel seguente modo:

```sh
gcc -std=c99 -Wall -Wpedantic -O2 -D_XOPEN_SOURCE=600 -DNO_SIMD -mavx -fopenmp omp-skyline.c  -lm -o omp-skyline
```

# Generazione degli input aggiuntivi

Per generare gli input utilizzati nel capitolo _2.6 Analisi delle performance_
della relazione eseguire le seguenti istruzioni:

```sh
chmode 700 ./input_gen_zero.sh
./input_gen_zero.sh
```

# Esecuzione

I programmi possono essere eseguiti nei segenti modi:

## versione CUDA

```sh
./cuda-skyline <input >output
```

## versione OMP (SIMD o non SIMD)

```sh
./omp-skyline <input >output
```
